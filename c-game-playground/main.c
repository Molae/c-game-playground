#include "header.h"

int main() {
    Team testTeam = ConstructorTeam("Test Team", 0);

    Player testPlayer = ConstructorPlayer("Test Player", 10);
    Player testPlayer2 = ConstructorPlayer("Second Test Player", 15);

    TeamAddPlayer(&testTeam, &testPlayer);
    TeamAddPlayer(&testTeam, &testPlayer2);

    printf("%-21s%s\n", "Test Player Name: ", testPlayer.Name);
    printf("%-21s%d\n", "Test Player Value: ", testPlayer.Value);

    printf("%-21s%s\n", "Test Player2 Name: ", testPlayer2.Name);
    printf("%-21s%d\n", "Test Player2 Value: ", testPlayer2.Value);

    printf("%-21s%s\n", "Test Team Name: ", testTeam.Name);
    printf("%-21s%d\n", "Test Team Value: ", testTeam.Value);

    printf("%-21s%s\n", "Test Team Player 0: ", testTeam.Players[0]->Name);
    printf("%-21s%s\n", "Test Team Player 1: ", testTeam.Players[1]->Name);

    TeamRemovePlayer(&testTeam, &testPlayer);

    printf("%-21s%s\n", "Test Team Name: ", testTeam.Name);
    printf("%-21s%d\n", "Test Team Value: ", testTeam.Value);

    printf("%-21s%s\n", "Test Team Player 0: ", testTeam.Players[0]->Name);

    return 0;
}
