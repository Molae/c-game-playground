//
// Created by Milton on 31-05-2022.
//

#ifndef C_GAME_PLAYGROUND_TEAM_H
#define C_GAME_PLAYGROUND_TEAM_H

#include <stdio.h>
#include <stdlib.h>
#include "player.h"

typedef struct Team {
    char* Name;
    int Value;
    Player** Players;
    int AmountPlayers;
} Team;

void CalculateTeamValue(Team *team);

Team ConstructorTeam(char* name, int value) {
    Team teamToReturn;

    teamToReturn.Name = name;
    teamToReturn.Players = malloc(0);
    teamToReturn.AmountPlayers = 0;

    CalculateTeamValue(&teamToReturn);

    return teamToReturn;
}

void CalculateTeamValue(Team *team) {
    team->Value = 0;
    for(int i = 0; i < team->AmountPlayers; i++) {
        team->Value += team->Players[i]->Value;
    }
}

void TeamAddPlayer(Team *team, Player *player) {
    // Reallocate the memory block with the players so we can fit the new player in the array
    realloc(team->Players, sizeof(Player*) * team->AmountPlayers + 1);

    //Add the player to the array of players and count the amount of players up
    team->Players[team->AmountPlayers] = player;
    team->AmountPlayers++;

    //Update the value of the team
    CalculateTeamValue(team);
}

void TeamRemovePlayer(Team *team, Player *player) {
    //Remove the player from the array of players and move the other players up in the array
    for (int i = 0; i < team->AmountPlayers; i++) {
        if (team->Players[i] == player) {
            team->Players[i] = NULL;
            for (int j = i; j < team->AmountPlayers - 1; j++) {
                team->Players[j] = team->Players[j + 1];
            }
            break;
        }
    }
    team->AmountPlayers--;

    // Reallocate the memory block with the players so we do not allocate memory for the removed player
    realloc(team->Players, sizeof(Player*) * team->AmountPlayers - 1);

    //Update the value of the team
    CalculateTeamValue(team);
}

#endif //C_GAME_PLAYGROUND_TEAM_H
