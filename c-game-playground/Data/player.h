//
// Created by Milton on 31-05-2022.
//

#ifndef C_GAME_PLAYGROUND_PLAYER_H
#define C_GAME_PLAYGROUND_PLAYER_H

typedef struct PlayerStats {
    // General Stats
    int Accuracy;
    int Shooting;
    int Dribbling;
    int Passing;

    // GK Stats
    int Handling;
    int Reflexes;
} PlayerStats;

typedef struct Player {
    char* Name;
    int Value;
    PlayerStats Stats;
} Player;

PlayerStats ConstructorPlayerStats() {
    PlayerStats statsToReturn;

    statsToReturn.Accuracy = 0;
    statsToReturn.Shooting = 0;
    statsToReturn.Dribbling = 0;
    statsToReturn.Passing = 0;

    statsToReturn.Handling = 0;
    statsToReturn.Reflexes = 0;

    return statsToReturn;
}

Player ConstructorPlayer(char* name, int value) {
    Player playerToReturn;

    playerToReturn.Name = name;
    playerToReturn.Value = value;
    playerToReturn.Stats = ConstructorPlayerStats();

    return playerToReturn;
}

Player ConstructorPlayerWithStats(char* name, int value, PlayerStats stats) {
    Player playerToReturn = ConstructorPlayer(name, value);

    playerToReturn.Stats = stats;

    return playerToReturn;
}

#endif //C_GAME_PLAYGROUND_PLAYER_H
